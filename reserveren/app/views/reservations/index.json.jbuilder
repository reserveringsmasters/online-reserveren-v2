json.array!(@reservations) do |reservation|
  json.extract! reservation, :table, :user, :date
  json.url reservation_url(reservation, format: :json)
end