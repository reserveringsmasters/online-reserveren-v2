json.array!(@tables) do |table|
  json.extract! table, :Seats
  json.url table_url(table, format: :json)
end