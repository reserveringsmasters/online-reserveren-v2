class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.integer :table
      t.integer :user
      t.datetime :date

      t.timestamps
    end
  end
end
