require 'spec_helper'

describe "StaticPages" do
  describe "Home page" do

    it "should have the content 'Sample App'" do
      visit root_path
      expect(page).to have_content('Sample App')
    end
     it "should have the title 'Home'" do
      visit root_path
      expect(page).to have_title("Ruby on Rails Tutorial Sample App | Home")
    end
  end
describe "Help page" do

    it "should have the content 'Help'" do
      visit root_path
      expect(page).to have_content('Help')
    end
    it "should have the title 'Help'" do
      visit root_path
      expect(page).to have_title("Ruby on Rails Tutorial Sample App | Help")
    end
  end

  describe "Reserveren page" do

    it "should have the content 'Reserveer een tafel'" do
      visit root_path
      expect(page).to have_content('Reserveer een tafel')
    end
    it "should have the title 'Reserveren'" do
      visit root_path
      expect(page).to have_title("Ruby on Rails Tutorial Sample App | Reserveren")
    end
  end
  
  describe "Contact page" do

    it "should have the content 'Contact'" do
      visit root_path
      expect(page).to have_content('Contact')
    end
  end
  
 describe "Menu page" do

    it "should have the content 'Menukaart'" do
      visit root_path
      expect(page).to have_content('Menukaart')
    end
  end 
  
end